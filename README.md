# Hi there <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px"> I'm Zakhar

- 🇷🇺 I live in Russia, Berdsk.
- 🤖 Most of the time I'm writing on Javascript, Node.JS, React.
- 🚀 I love pet-projects. They help me grow professionally all my life.

## 🛠 My fancy badge area

![js](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![nodejs](https://img.shields.io/badge/node.js-%2343853D.svg?style=for-the-badge&logo=node-dot-js&logoColor=white)
![docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)
![typescript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![expressjs](https://img.shields.io/badge/express.js-%23404d59.svg?style=for-the-badge&logo=express&logoColor=%2361DAFB)
![reactjs](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![webpack](https://img.shields.io/badge/webpack-%238DD6F9.svg?style=for-the-badge&logo=webpack&logoColor=black)
![eslint](https://img.shields.io/badge/ESLint-4B3263?style=for-the-badge&logo=eslint&logoColor=white)
![webstorm](https://img.shields.io/badge/webstorm-143?style=for-the-badge&logo=webstorm&logoColor=white&color=black)
![vscode](https://img.shields.io/badge/VisualStudioCode-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white)
![visualstudio](https://img.shields.io/badge/VisualStudio-5C2D91.svg?style=for-the-badge&logo=visual-studio&logoColor=white)
![git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)
![github](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)
![nginx](https://img.shields.io/badge/nginx-%23009639.svg?style=for-the-badge&logo=nginx&logoColor=white)
![mysql](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)
![postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)
![mongodb](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white)
![redis](https://img.shields.io/badge/redis-%23DD0031.svg?style=for-the-badge&logo=redis&logoColor=white)
![githubactions](https://img.shields.io/badge/githubactions-%232671E5.svg?style=for-the-badge&logo=githubactions&logoColor=white)
![mocha](https://img.shields.io/badge/-mocha-%238D6748?style=for-the-badge&logo=mocha&logoColor=white)
![ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white)
![windows](https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white)
![android](https://img.shields.io/badge/Android-3DDC84?style=for-the-badge&logo=android&logoColor=white)

### P.S I'm not a fan of the GitLab cloud service, so if you need more information, then go to my [GitHub profile](https://github.com/ZakharYA).

## Social
<a href="https://github.com/ZakharYA">
  <img align="left" src="https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white" alt="GitHub" />
</a>
<a href="https://twitter.com/Zakhar_YP">
  <img align="left" src="https://img.shields.io/badge/Zakhar_YP-%231DA1F2.svg?style=for-the-badge&logo=Twitter&logoColor=white" alt="Twitter" />
</a>
<a href="https://vk.com/ghost1337gg">
  <img height="28" align="left" src="https://uc0dc57c5a0096d7178161b4ff9e.previews.dropboxusercontent.com/p/thumb/ABelUyMrXvqhkv-9NhfV88sVCvDXk0PWjSVj7f7OrlRLfY0wueOd1hXSwSwVOEkC9-RndM5bwg9BAREuHNrUKtQL3p4MT3q2UQAg0mLRjELQiELRtmdI4F-tVrQGK54L37YCmmkajRFKtlMQ6n8AOBnsHpiF8aHqhXRFN5KSu62LUEOepOxysmR5-treRbmSP6HwzrAuykFmqGFDD6kYVa3Bm5D_BGoI9thfNDjkY_Yyd78j_EYNr6Xg0dHKmDBSE8BnoVLCMThPI31IwUV4ZI_Ig0CMwynBH0pD0iknPSd3JrGYuGp4dxVMhgf7WqPGH5mbaiuzwjOyKALYuWVDL-uWPX3KyyRlppM8TM-oXxKBG9MnMdXkEu5IqCIzds-n_4wQl_SdXj4pyXAfgeZ5NDmEQ734P2DrtKPEYvdFfTx34g/p.png" alt="VK" />
</a>
<a href="https://linkedin.com/in/zakhar-yaitskikh-a52a79213">
  <img align="left" src="https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white" alt="Linkedin" />
</a>
<a href="https://t.me/ghost1337gg">
  <img align="left" src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white" alt="Telegram" />
</a>

